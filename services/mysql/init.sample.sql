# services/mysql/init.sql
use mysql;
CREATE USER 'MYSQL_USER'@'%' IDENTIFIED BY 'MYSQL_PASSWD';
create database PROJECT_NAME default character set utf8mb4  COLLATE utf8mb4_unicode_ci;
grant all on PROJECT_NAME.* to 'MYSQL_USER'@'%';