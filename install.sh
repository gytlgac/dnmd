#!/bin/bash

# 安装、运行

#替换windows 中的换行符
sed -i "s/\r//" ./env.sample

source ./env.sample
echo "projectname=$PROJECT_NAME"

# mysql 配置
echo "开始对mysql 进行配置、处理，替换项目名称、设置用户名、口令等"
mkdir -p ./services/mysql/init
cp -f ./services/mysql/init.sample.sql ./services/mysql/init/init.sql
sed -i "s/PROJECT_NAME/$PROJECT_NAME/g" ./services/mysql/init/init.sql
sed -i "s/MYSQL_USER/$MYSQL_USER/g" ./services/mysql/init/init.sql
sed -i "s/MYSQL_PASSWD/$MYSQL_PASSWD/g" ./services/mysql/init/init.sql
echo "结束对mysql 进行配置、处理"

# nginx 配置替换
echo "开始对nginx进行配置、处理，主要是修改localhost.conf配置文件"
cp -f ./services/nginx/conf.d/localhost.conf.sample ./services/nginx/conf.d/localhost.conf
sed -i "s/PROJECT_NAME/$PROJECT_NAME/g" ./services/nginx/conf.d/localhost.conf
echo "结束对nginx 进行配置、处理"

# django 配置
echo "开始构建 django 运行镜像。。。。"
echo "1、拷贝.dockerignore文件，并替换其中的变量"
cp -f services/python/.dockerignore www/${PROJECT_NAME}
sed -i "s/PROJECT_NAME/$PROJECT_NAME/g" ./www/${PROJECT_NAME}/.dockerignore
echo "2、拷贝Dockerfile文件，并替换其中的变量"
cp -f services/python/Dockerfile www/${PROJECT_NAME}
sed -i "s/PROJECT_NAME/$PROJECT_NAME/g" ./www/${PROJECT_NAME}/Dockerfile
echo "3、添加pip 源、start.sh文件、uwsgi配置文件 "
cp -f services/python/pip.conf www/${PROJECT_NAME}
cp -f services/python/start.sh www/${PROJECT_NAME}
sed -i "s/PROJECT_NAME/$PROJECT_NAME/g" ./www/${PROJECT_NAME}/start.sh
cp -f services/python/uwsgi.ini www/${PROJECT_NAME}
sed -i "s/PROJECT_NAME/$PROJECT_NAME/g" ./www/${PROJECT_NAME}/uwsgi.ini
echo "结束django 的配置"

cp -f ./docker-compose.sample.yml ./docker-compose.yml 
cp -f ./env.sample ./.env

docker-compose up 