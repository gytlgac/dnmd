# dnmd

## 介绍
&nbsp;&nbsp;&nbsp;&nbsp;因为centos已经停服，按要求要迁移到新的可持续更新的系统中去。在网上找了一下，`rocky linux 8.6`已经可用，所以新的系统准备采用`rocky linux`作为新的操作系统。在尝试迁移时发现以前做的很多项目，在迁移时因为环境和操作版本的变化很麻烦，而使用docker部署是一种非常成熟的方案，所以决定使用docker开部署以前开发的项目。

&nbsp;&nbsp;&nbsp;&nbsp;在网上找了一下，有许多使用docker或docker-compse构建django运行环境的，但是对不同的项目使用上要修改的地方较多，通用性不强，所以想做一个通用性比较强、使用方便、修改地方少、使用docker-compose构建django运行环境的开源项目。

本项目参考了开源项目[yeszao/dnmp](https://gitee.com/yeszao/dnmp)

## 软件架构
- Docker Compose version v2.2.2
- Django==4.0.6
- python 3.8.8

## 安装教程
&nbsp;&nbsp;&nbsp;&nbsp;使用本项目和通常的django项目开发区别不大，只需要做简单的修改就可以将一个已经开发的django项目以docker-compose构建运行环境。
### 1.  git clone https://gitee.com/gytlgac/dnmd.git

### 2.  将django项目整个copy到`./www`目录下

### 3.  修改变量文件 `env.sample`

&nbsp;&nbsp;&nbsp;&nbsp;该文件存储了整个项目中要使用到的变量，我们需要修改文件中的PROJECT_NAME变量的值，比如我的django项目为mysite，则需要在文件中写入PROJECT_NAME=mysite。**要注意区分大小写**

### 4.  修改./services/python/.dockerignore文件

&nbsp;&nbsp;&nbsp;&nbsp;该文件是用于将项目文件拷贝到django镜像时忽略不需拷贝的文件或目录时用的，默认是不拷贝任何文件，如果需要将某目录进行拷贝，请在文件中添加`!目录名` (注意是英文的!)，如：我要将`apps`目录进行拷贝，在文件的新行中添加`!apps`

### 4.  修改`env.sample`文件中关于`mysql`的配置项
```conf
MYSQL_HOST_PORT=3308    # mysql 对外端口
MYSQL_ROOT_PASSWORD=123456  # root用户密码，建议修改
MYSQL_ROOT_HOST=%   # 默认允许任意主机访问
MYSQL_USER=dbuser   # django项目中访问数据库的用户名，不建议使用root用户，该用户要与项目中的配置一致
MYSQL_PASSWD=NGZytUGkS1Sd # django项目中访问数据库的用户密码
```
### 5. 运行install.sh脚本

```shell
cd dnmd
sh install.sh
```

## 使用说明

### 1.  数据库管理 
#### 1.1 phpMyAdmin
phpMyAdmin容器映射到主机的端口地址是：8080，所以主机上访问phpMyAdmin的地址是：
```
    http://localhost:8080
```
MySQL连接信息：

- host：(本项目的MySQL容器网络)
- port：3306
- username：（手动在phpmyadmin界面输入）
- password：（手动在phpmyadmin界面输入）



## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
